new Function('return this')().Element = class ElementMock {}
const basePath =
	'development' === process.env.NODE_ENV ? '' : '/react-window-20210902/'

module.exports = {
	basePath: basePath.replace(/\/$/, ''),
	assetPrefix: basePath,
	publicRuntimeConfig: {
		basePath,
	},
	trailingSlash: true,
}
