import { memo, CSSProperties, useRef, useEffect } from 'react'
import { FixedSizeList, VariableSizeList, areEqual } from 'react-window'
import {
	is,
	Cells,
	CodeLine,
	TContext,
	useCtx,
	useSetScrollHeight,
	useSetScrollWidth,
	useScroll,
} from './table'
import style from './table-body.module.sass'

type ListItemProps<T> = Readonly<{
	index: number
	style: CSSProperties
	data: T
}>

const CellsTLItem = memo(
	(props: ListItemProps<Cells>) => (
		<div className={style.td} style={props.style}>
			{props.data[props.index] || ''}
		</div>
	),
	areEqual,
)

const CellsHeadTLItem = memo(
	(props: ListItemProps<Cells>) => (
		<div className={style.th} style={props.style}>
			{props.data[props.index] || ''}
		</div>
	),
	areEqual,
)

const CellsTL = memo(
	(
		props: Readonly<{
			line: Cells
			CellsTLItem: { (p: ListItemProps<Cells>): JSX.Element | null }
		}>,
	) => {
		const { trStyle, fontSize, clientWidth, getColSize } = useCtx()
		const scroll = useScroll()
		const rr = useRef<VariableSizeList<Cells>>(null)
		const setScroll = useSetScrollWidth(rr)
		useEffect(() => {
			scroll.lastW !== rr.current && rr.current?.scrollTo(scroll.width)
		}, [scroll.width | 0])
		return (
			<div className={style.tr} style={trStyle}>
				<VariableSizeList<Cells>
					height={fontSize * 1.5}
					itemCount={props.line.length}
					itemSize={getColSize}
					itemData={props.line}
					width={clientWidth}
					layout="horizontal"
					onScroll={setScroll}
					ref={rr}
				>
					{props.CellsTLItem}
				</VariableSizeList>
			</div>
		)
	},
	(p, n) => p.line === n.line,
)

const DividerTL = memo(
	(_: Readonly<{ _?: unknown }>) => {
		const { trStyle } = useCtx()
		return (
			<div className={style.tr} style={trStyle}>
				<hr />
			</div>
		)
	},
	() => true,
)

const CodeTL = memo(
	(props: Readonly<{ line: CodeLine }>) => {
		const { trStyle } = useCtx()
		return (
			<div className={style.tr} style={trStyle}>
				<pre>
					<code>{props.line.code}</code>
				</pre>
			</div>
		)
	},
	(p, n) => p.line.code === n.line.code,
)

export const TableHead = memo(
	() => {
		const { head } = useCtx()
		return (
			<div>
				{head.map((line, i) =>
					is.cells(line) ? (
						<CellsTL key={i} line={line} CellsTLItem={CellsHeadTLItem} />
					) : (
						<CodeTL key={i} line={line} />
					),
				)}
			</div>
		)
	},
	() => true,
)

const TableBodyItem = memo((props: ListItemProps<TContext['body']>) => {
	const line = props.data[props.index]
	if (!line) return null
	return (
		<div style={props.style}>
			{is.cells(line) ? (
				<CellsTL line={line} CellsTLItem={CellsTLItem} />
			) : is.divider(line) ? (
				<DividerTL />
			) : (
				<CodeTL line={line} />
			)}
		</div>
	)
}, areEqual)

export const TableBody = memo(
	() => {
		const { body, fontSize, clientWidth } = useCtx()
		const setScroll = useSetScrollHeight()
		return (
			<FixedSizeList<TContext['body']>
				height={300}
				itemCount={body.length}
				itemSize={fontSize * 1.5}
				itemData={body}
				width={clientWidth}
				onScroll={setScroll}
			>
				{TableBodyItem}
			</FixedSizeList>
		)
	},
	() => true,
)
