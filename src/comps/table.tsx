import {
	CSSProperties,
	memo,
	RefObject,
	useCallback,
	useEffect,
	useMemo,
	useRef,
	useState,
} from 'react'
import { useContext } from 'react'
import { createContext } from 'react'
// import {} from 'react-virtuoso'
import type { VariableSizeList } from 'react-window'
import style from './table.module.sass'

export type Line = Cells | Divider | CodeLine

export type Cells = readonly string[]
export type Divider = Readonly<{ is: 'divider' }>
export type CodeLine = Readonly<{ is: 'code'; code: string }>

const isReadonlyArray: (v: unknown) => v is readonly unknown[] = Array.isArray

export const is = {
	cells: isReadonlyArray as (v: Line) => v is Cells,
	divider: (v: Line): v is Divider => 'divider' === (v as { is: string }).is,
	codeLine: (v: Line): v is CodeLine => 'code' === (v as { is: string }).is,
} as const

export type TContext = Readonly<{
	head: readonly (Cells | CodeLine)[]
	body: readonly (Cells | Divider | CodeLine)[]
	fontSize: number
	totalWidth: number
	clientWidth: number
	trStyle: CSSProperties
	tableStyle: CSSProperties
	getRowSize: (idx: number) => number
	getColSize: (idx: number) => number
}>

const ctx = createContext<TContext | null>(null)
export const useCtx = (): TContext => {
	const s = useContext(ctx)
	if (!s) throw new Error('no context found.')
	return s
}
const CtxProvider = ctx.Provider

type TProps = Readonly<{
	lines: readonly Line[]
	/** 桁数 */
	digitnums: readonly number[]
}>

const useCalculatedSizes = (
	digitnums: readonly number[],
	fontSize: number | null,
) => {
	return useMemo(() => {
		if (!fontSize)
			return {
				trStyle: {},
				tableStyle: { width: '100%' },
				totalWidth: 1000,
				getRowSize: () => 0,
				getColSize: () => 0,
			}
		const mean = digitnums?.length
			? digitnums.reduce((q, w) => q + w, 0) / digitnums.length
			: 1
		const sizes = digitnums?.map(d => Math.ceil((d || mean) / 2)) || []
		const trStyle = {
			gridTemplateColumns: ([] as string[])
				.concat(
					sizes.map(num =>
						Number.isFinite(num) ? num * fontSize + 'px' : '1fr',
					),
				)
				.join(' '),
		}
		const totalWidth = sizes.reduce((q, num) => q + num * fontSize, 0)
		const tableStyle = {
			// width: `${totalWidth}px`,
			width: '100%',
		}
		const getRowSize = (_: number) => fontSize
		const getColSize = (idx: number) => (sizes[idx] || 0) * fontSize
		return { trStyle, tableStyle, getRowSize, getColSize, totalWidth }
	}, [digitnums, fontSize])
}

const unreachable = (_: never): never => {
	throw new Error('unreachable.')
}

const useHeadBody = (props: TProps) => {
	return useMemo(() => {
		const head: (Cells | CodeLine)[] = []
		const body: (Cells | Divider | CodeLine)[] = []
		type Mode = 'read-head' | 'read-body'
		let mode: Mode = 'read-head'
		for (const line of props.lines) {
			switch (mode) {
				case 'read-head':
					if (is.cells(line)) {
						head.push(line)
					} else if (is.codeLine(line)) {
						head.push(line)
					} else if (is.divider(line)) {
						mode = 'read-body'
					} else {
						return unreachable(line)
					}
					break
				case 'read-body':
					body.push(line)
					break
				default:
					return unreachable(mode)
			}
		}
		return { head, body }
	}, [props.lines])
}

type SCtx = Readonly<{
	width: number
	height: number
	lastW?: VariableSizeList
}>
const scrollCtx = createContext<SCtx>({ width: 0, height: 0 })
const scrollSetterCtx = createContext<{ (f: { (s: SCtx): SCtx }): void }>(
	() => undefined,
)

export const useScroll = () => useContext(scrollCtx)
export const useScrollSetter = () => useContext(scrollSetterCtx)
export const useSetScrollWidth = (rr: RefObject<VariableSizeList>) => {
	const setScroll = useScrollSetter()
	return useCallback(
		(e: {
			scrollDirection: 'forward' | 'backward'
			scrollOffset: number
			scrollUpdateWasRequested?: boolean
		}) => {
			if ('forward' === e.scrollDirection && 0 === e.scrollOffset) return
			if (e.scrollUpdateWasRequested) return
			setScroll(s => ({
				...s,
				width: e.scrollOffset,
				lastW: rr.current || undefined,
			}))
		},
		[setScroll],
	)
}
export const useSetScrollHeight = () => {
	const setScroll = useScrollSetter()
	return useCallback(
		(e: {
			scrollDirection: 'forward' | 'backward'
			scrollOffset: number
			scrollUpdateWasRequested?: boolean
		}) => {
			if ('forward' === e.scrollDirection && 0 === e.scrollOffset) return
			if (e.scrollUpdateWasRequested) return
			setScroll(s => ({ ...s, height: e.scrollOffset }))
		},
		[setScroll],
	)
}

const TableContent = memo(
	(
		props: Readonly<{
			TableHead: { (p: Record<string, never>): JSX.Element | null }
			TableBody: { (p: Record<string, never>): JSX.Element | null }
		}>,
	) => {
		const [sCtx, setSCtx] = useState<SCtx>({ width: 0, height: 0 })
		return (
			<scrollCtx.Provider value={sCtx}>
				<scrollSetterCtx.Provider value={setSCtx}>
					<div className={style.thead}>
						<props.TableHead />
					</div>
					<div className={style.tbody}>
						<props.TableBody />
					</div>
				</scrollSetterCtx.Provider>
			</scrollCtx.Provider>
		)
	},
	(p, n) => p.TableHead === n.TableHead && p.TableBody === n.TableBody,
)

const useClientWidth = () => {
	const [size, setSize] = useState<number>(
		'undefined' === typeof document ? 640 : document.body.clientWidth,
	)
	useEffect(() => {
		const handler = () => {
			const clientWidth =
				'undefined' === typeof document ? 640 : document.body.clientWidth
			setSize(clientWidth)
		}
		document.body.addEventListener('resize', handler)
		return () => {
			document.body.removeEventListener('resize', handler)
		}
	}, [setSize])
	return size
}

type TableProps = TProps &
	Readonly<{
		TableHead: { (p: Record<string, never>): JSX.Element | null }
		TableBody: { (p: Record<string, never>): JSX.Element | null }
	}>

export const Table = memo(
	({ TableHead, TableBody, ...props }: TableProps) => {
		const wrapDivRef = useRef<HTMLDivElement>(null)
		const [fontSize, setFontSize] = useState<null | number>(null)
		useEffect(() => {
			if (wrapDivRef.current) {
				const div = wrapDivRef.current
				const fontSize = getComputedStyle(div).fontSize
				if (/^\d+px$/u.test(fontSize))
					setFontSize(Number(fontSize.replace(/px$/u, '')))
				else alert('error.')
			}
		}, [setFontSize])
		const s = useCalculatedSizes(props.digitnums, fontSize)
		const hb = useHeadBody(props)
		const clientWidth = useClientWidth()
		return (
			<div className={style.table} style={s.tableStyle} ref={wrapDivRef}>
				{null === fontSize ? null : (
					<CtxProvider value={{ ...s, ...hb, fontSize, clientWidth }}>
						<TableContent TableHead={TableHead} TableBody={TableBody} />
					</CtxProvider>
				)}
			</div>
		)
	},
	(p, n) =>
		p.TableHead === n.TableHead &&
		p.TableBody === n.TableBody &&
		p.digitnums === n.digitnums &&
		p.lines === n.lines,
)

export default Table
