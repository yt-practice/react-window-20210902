import { useEffect, useRef } from 'react'
import style from './sandbox.module.sass'

export const Sandbox = () => {
	const d1 = useRef<HTMLDivElement>(null)
	const d2 = useRef<HTMLDivElement>(null)
	const d3 = useRef<HTMLDivElement>(null)
	useEffect(() => {
		if (d1.current && d2.current && d3.current) {
			const els = [d1.current, d2.current, d3.current].map(el => {
				return {
					fontSize: getComputedStyle(el).fontSize,
					height: getComputedStyle(el).height,
					width: getComputedStyle(el).width,
				}
			})
			console.log(els)
		}
	}, [d1, d2, d3])
	return (
		<div className={style.wrap}>
			<p ref={d1} className={style.test1}>
				test
			</p>
			<br />
			<p ref={d2} className={style.test2}>
				test
			</p>
			<br />
			<p ref={d3} className={style.test3}>
				test
			</p>
		</div>
	)
}

export default Sandbox
