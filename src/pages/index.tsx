import { memo } from 'react'
import Sandbox from '~/comps/sandbox'
import Table from '~/comps/table'
import { TableBody, TableHead } from '~/comps/table-body'

const BigTable = memo(
	() => {
		console.log('render list.')
		const range = (length: number) => Array.from({ length }, (_, i) => 1 + i)
		const widthCount = 320
		const heightCount = 120000
		return (
			<Table
				lines={[
					['id', 'name', 'age', 'sex', ...range(widthCount).map(i => `p${i}`)],
					{ is: 'divider' },
					...range(heightCount).map(idx => {
						if (0 === idx % 100) return { is: 'divider' } as const
						return [
							`${idx}`,
							`hoge${idx}`,
							'12',
							'male',
							...range(widthCount).map(i => `d${idx}.${i}`),
						]
					}),
				]}
				digitnums={[6, 12, 10, 10, ...range(widthCount).map(() => 16)]}
				TableBody={TableBody}
				TableHead={TableHead}
			/>
		)
	},
	() => true,
)

const Home = () => {
	return (
		<div>
			<Sandbox />
			<hr />
			<BigTable />
		</div>
	)
}

export default Home
